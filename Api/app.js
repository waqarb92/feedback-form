const express = require("express");
const app = express();

// Enable CORS
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// Middleware to parse passed data to JSON
app.use(express.json());

//Routes
app.post('/post-form', (req, res) => {
  console.log('form data:', req.body);
  res.sendStatus(200);
});

//Listen
app.listen(8080);
