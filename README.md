The API and Client side have been built seperately. To get both running:

# Install instructions

### `cd /Api or /client`

### `npm install`

### `npm start`


# Additional packages used:

React App was start with create react app with the addition of node-sass and propTypes

NodeJS Server makes use of express and nodemon for detecting file changes and restarting the server


# Additional information: 

React app was created specifically for this form, for this reason I did not use a great deal of classes to define my elements, rather styled everything specifically as needed via the element itself and the use of parent, children selectors with the help of SCSS to make it simpler to write and read.


NodeJS Server was created manually adding and removing packages as required. 


I tried to use as little additional technology and packages as possible, although create react app installed more than was required, it may have been a better option to build the whole react app from scratch but time was not permitting.


# Show error page

To show the error page which would display if there is a problem with the form being submitted, this can be tested by turning off the API and trying to submit the form.