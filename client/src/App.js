import React, {useState, Fragment} from 'react';
import { useForm } from "react-hook-form";
import Step1 from './components/Step1';
import Step2 from './components/Step2';
import ThankYou from './components/ThankYou';
import Error from './components/Error';
import './App.scss';

function App() {
  // Create state object for current step and setting initial step to 1
  const [currentStep, setStep] = useState(1);
  const { register, errors, handleSubmit, formState } = useForm({
    mode: "onBlur"
  });

  // Form submit function
  const onSubmit = data => {
    // Post form data to exposed API
    fetch('http://localhost:8080/post-form', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data)
    }).then((response) => {
      // If server responds with 200 show thank you else show error
      if (response.status === 200) {
        setStep(3);
        // Show Thank you page
      } else {
        setStep(4);
        // Show Error Page
      }
    }).catch(error => {
      setStep(4);
      // Show error page
    });
  };

  // Form errors and touched state console logged for testing
  // console.log(errors);
  // console.log(formState.touched);

  return (
    <div className="App">
    <h1>Survey</h1>
      {currentStep <= 2 ?
        <Fragment>
          <form className="Feedback" onSubmit={handleSubmit(onSubmit)}>
          <Step1 step={currentStep} nextStep={setStep} errors={errors} register={register} touched={formState.touched} />
          <Step2 step={currentStep} register={register} errors={errors} touched={formState.touched} isValid={formState.isValid} />
          </form>
        </Fragment>
        :
        <Fragment>
          {currentStep === 3 ? 
            <ThankYou step={currentStep} />
            :
            null
          }
          {currentStep === 4 ? 
            <Error step={currentStep} />
            :
            null
          }
        </Fragment>
      }
    </div>
  );
}

export default App;
