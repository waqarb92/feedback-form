import React from 'react';
function Error() {

  return (
    <div className="Error">
        <h2> An Error has occured </h2>
        <p> We could not deal with your request at this time please try again later.</p>
    </div>
  );
}

export default Error;
