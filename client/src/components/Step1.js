import React, {useState} from 'react';
import PropTypes from 'prop-types';

function Step1(props) {
  const { step, nextStep, register, errors, touched } = props;
  const [showError, hasError] = useState(false);

  const goToNextStep = () => {
    if (Object.keys(touched).length >= 5 & Object.keys(errors).length <= 0) {
      nextStep(2);
    } else {
      hasError(true);
    }
  }

  return (
    <div className={"Step1 " + (step !== 1 ? 'inactive' : '')}>
    {showError && <p className="error">All of this section must be completed before moving on</p>}
      <label htmlFor="Title">
        <span>Title</span>
        <select id="Title" name="Title" ref={register}>
          <option value=""></option>
          <option value="Mr">Mr.</option>
          <option value="Ms">Ms.</option>
          <option value="Mrs">Mrs.</option>
          <option value="Dr">Dr.</option>
        </select>
      </label>

      <label htmlFor="Name">
      <span>Name *</span>
      <input 
        type="text"
        id="Name"
        name="Name" 
        placeholder="John Doe" 
        ref={register({ required: true })} />
      </label>
      {errors.Name && touched.Name && <p className="error">Name is required</p>}

      <div className="DOB">
        <label htmlFor="Month">
          <span>Month *</span>
          <input id="Month" type="number" name="DOBMonth" placeholder="01" ref={register({ required: true })}/>
        </label>
        <label htmlFor="Day">
          <span>Day *</span>
          <input type="number" name="DOBDay" placeholder="01" ref={register({ required: true })} />
        </label>
        <label htmlFor="Year">
          <span>Year *</span>
          <input type="number" id="Year" name="DOByear" placeholder="2020" ref={register({ required: true })} />
        </label>
      </div>
      {
        (errors.DOBMonth && touched.DOBMonth) || 
        (errors.DOBDay && touched.DOBDay) || 
        (errors.DOBYear && touched.DOBYear) 
        ? 
        <p className="error">Date of Birth is required</p> 
        : null
      }

      <button type="button" onClick={() => goToNextStep()}>Next</button>
    </div >
  );
}

Step1.propTypes = {
  step: PropTypes.number,
  register: PropTypes.func.isRequired,
  nextStep: PropTypes.func.isRequired,
  touched: PropTypes.object.isRequired,
  errors: PropTypes.object,
}


// Default prop so element shows by default if no value passed.
Step1.defaultProps = {
  step: 1,
  errors: {}
}

export default Step1;
