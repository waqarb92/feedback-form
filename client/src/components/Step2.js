import React from 'react';
import PropTypes from 'prop-types';

function Step2(props) {
  const { step, register, errors, touched } = props;

  // Get current date and time for when form was filled out.
  const getDateTime = () => {
    const currentdate = new Date();
    // Create string with date and time, string to be DD/MM/YYYY @ HH:MM:SS 
    const datetime = currentdate.getDate() + "/"
                    + (currentdate.getMonth()+1)  + "/" 
                    + currentdate.getFullYear() + " @ "  
                    + currentdate.getHours() + ":"  
                    + currentdate.getMinutes() + ":" 
                    + currentdate.getSeconds();
  return datetime;
  }
  
  return (
    <div className={"Step2 " + (step !== 2 ? 'inactive' : '')}>
      <label htmlFor="Location">
        <span>Location *</span>
        <input 
          type="text"
          id="Location" 
          name="Location" 
          placeholder="Birmingham, UK" 
          ref={register({ required: true })}/>
      </label>
      {(errors.Location && touched.Location) && <p className="error">Location is required</p>}

      {/* Hidden input to record current date and time and submit it as part of the form */}
      <input type="text" name="dateTime" value={getDateTime()} className="hidden" ref={register} readOnly/>

      <label htmlFor="Feedback">
      <span>User Feedback *</span>
      <textarea 
        id="Feedback"
        name="Feedback" 
        placeholder="Feedback here...." 
        ref={register({ required: true })} />
      </label>
      {(errors.Feedback && touched.Feedback) && <p className="error">Feedback is required</p>}

      <input type="submit"/>
    </div>
  );
}

Step2.propTypes = {
  step: PropTypes.number,
  errors: PropTypes.object,
  register: PropTypes.func.isRequired,
  touched: PropTypes.object.isRequired,
}

// Default prop so element shows by default if no value passed.
Step2.defaultProps = {
  step: 2,
  errors: {}
}

export default Step2;
