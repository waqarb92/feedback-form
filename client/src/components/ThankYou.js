import React from 'react';

function ThankYou() {

  return (
    <div className="ThankYou">
        <h2> Thank You </h2>
        <p> Thank you for your feedback we will be in touch shortly.</p>
    </div>
  );
}

export default ThankYou;
